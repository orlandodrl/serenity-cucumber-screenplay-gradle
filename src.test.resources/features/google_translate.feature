
Feature: Google Translate
  As a web user
  I want to use Google Translate
  to translate different words

  @tag1
  Scenario: Translate from Spanish to Tamil
    Given that juanita opened her browser at google home page
    And she goes to Google Translate
    When she translates the word naranja from Español to Inglés
    Then she should see the word orange in the screen

  @tag2
  Scenario: Translate from Tamil to Esperanto
    Given that juanita opened her browser at google home page
    And she goes to Google Translate
    When she translates the word ஆரஞ்சு from Tamil to Esperanto
    Then she should see the word oranĝa in the screen
