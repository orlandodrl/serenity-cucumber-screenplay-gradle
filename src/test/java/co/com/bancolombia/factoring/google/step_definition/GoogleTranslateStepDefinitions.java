package co.com.bancolombia.factoring.google.step_definition;

//import org.junit.Before;
import org.openqa.selenium.WebDriver;

import co.com.bancolombia.factoring.google.model.TranslateExpression;
import co.com.bancolombia.factoring.google.questions.TheResult;
import co.com.bancolombia.factoring.google.tasks.GoTo;
import co.com.bancolombia.factoring.google.tasks.OpenTheBrowser;
import co.com.bancolombia.factoring.google.tasks.Translate;
import static co.com.bancolombia.factoring.google.user_interfaces.GoogleApps.GOOGLE_TRANSLATE;
import co.com.bancolombia.factoring.google.user_interfaces.GoogleHomePage;
import co.com.bancolombia.factoring.google.user_interfaces.GoogleTranslatePage;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class GoogleTranslateStepDefinitions {
	
	@Managed(driver = "chrome")
	private WebDriver herBrowser;
	
	private Actor juanita = Actor.named("Juanita");
	
	private GoogleHomePage googleHomePage;
	
	@Before
	public void setup() {
		juanita.can(BrowseTheWeb.with(herBrowser));   // Se le asigna el driver al actor
	}
	
	// wasAbleTo (given) (Precondicion) - attemptsTo (when)  (Condicion)
	
	@Given("^that (.*) opened her browser at google (.*)$")
	public void thatTheUserOpenedTheBrowserAt(String name, String webpage) throws Exception {
		juanita.wasAbleTo(OpenTheBrowser.at(googleHomePage));   
	}

	@And("^she goes to (.*)$")
	public void sheGoesTo(String googleapp) throws Exception {
		juanita.wasAbleTo(GoTo.theApp(GOOGLE_TRANSLATE)); 
	}

	@When("^she translates the word (.*) from (.*) to (.*)$")
	public void sheTranslatesTheWordFromTo(String word, String sourceLanguage, String targetLanguage) throws Exception {
		juanita.attemptsTo(Translate.the(new TranslateExpression(sourceLanguage, targetLanguage, word)));
	}

	@Then("^she should see the word (.*) in the screen$")
	public void sheShouldSeeTheWordInTheScreen(String expectedWord) throws Exception {
		juanita.should(seeThat(TheResult.is(), equalTo(expectedWord)));
	}

}
